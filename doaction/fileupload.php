<?php

class FileUpload implements Doaction {
    private $result='';
    
    public function execute() {
        /* Add required CSS */
        clsAddScript::addCSS('fileinput');
        
        /* Add required JS */
        clsAddScript::addJS('canvas-to-blob');
        clsAddScript::addJS('fileinput');
        clsAddScript::addJS('fileinput-config');
    }
    
    public function Body() {
        $this->result =  clsHtml::h(1,'Welcome to file upload');
        $this->result .= clsHtml::startForm();
            $this->result .= clsHtml::textField('fileupload[]', 'fileupload', ['id'=>"input-id", 'type'=>"file", 'class'=>"file", 'data-preview-file-type'=>"text", 'multiple'=>true]);
        $this->result .= clsHtml::endForm(true, 'Upload', ['value'=>'save-fileupload']);
        
    }
    
    public function renderBody() {
        return $this->result;
    }
    
    public function handleFileUpload() {
        $this->result .= clsHtml::alert('Handling file upload', TRUE);
        $this->uploadFile();
    }
    
    private function uploadFile() {
        $images = reArrayFiles($_FILES['fileupload']);
        foreach($images as $image) {
            if(move_uploaded_file($image['tmp_name'], IMG_DIR . basename($image['name']))) {
                $this->alert('The file '.basename($image['name'].' has been uploaded'));
            }
        }
    }
    
    private function alert($msg) {
        $this->result .= clsHtml::alert($msg, true);
    }
    
}