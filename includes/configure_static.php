<?php

define('BAS_DIR', '/websocket');
define('JS_DIR', BAS_DIR . '/js/');
define('CSS_DIR', BAS_DIR . '/css/');
define('IMG_DIR', realpath(dirname(__FILE__).'/../images/').'/');

$arJsFilesNames = [
    'jquery'=>'jquery.js',
    'bootstrap'=>'bootstrap.js',
    'chat'=>'chat.js',
    'fileinput'=>'fileinput.min.js',
    'canvas-to-blob'=>'plugins/canvas-to-blob.min.js',
    'fileinput-config'=>'fileinput-config.js',
];

$arCssFilesNames = [
    'bootstrap'=>'bootstrap.css',
    'style'=>'style.css',
    'fileinput'=>'fileinput.min.css',
];

