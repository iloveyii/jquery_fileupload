<?php

interface Doaction  {
    /**
     * This function is executed to do inital stuff eg getting data from db
     * It works like Model
     */
    public function execute();
    
    /**
     * This prepare the html page
     * This works like View
     */
    public function body();
    
    /**
     * This function gets data from class variables and echo it
     * This works like renderView
     */
    public function renderBody();
}


?>