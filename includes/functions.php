<?php
/**
 * Get a value from REQUEST
 * 
 * @param string $fieldName
 * @return string
 * @author Lars Wallgren <lars@egrannar.com>
 */
function getValue($fieldName = ""){
    if(isset($_REQUEST[$fieldName])){
        if (is_string($_REQUEST[$fieldName])) {
            return trim($_REQUEST[$fieldName]);
        } else {
            return $_REQUEST[$fieldName];
        }
    }else{
        return "";
    }
}

/**
 * Separates files array into separate own arrays
 * 
 * @param array $arFilepost
 * @return array
 * @author Lars Wallgren <lars@egrannar.com>
 */
function reArrayFiles($arFilepost) {

    $arFile = array();
    
    $intFilecount = count($arFilepost['name']);
    if (is_array($arFilepost)) {
        $arFilekeys = array_keys($arFilepost);

        for ($i=0; $i<$intFilecount; $i++) {
            foreach ($arFilekeys as $key) {
                $arFile[$i][$key] = $arFilepost[$key][$i];
            }
        }
    }

    return $arFile;
}

?>