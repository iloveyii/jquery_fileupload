<?php

// =====================================================================================
// Class clsHtml
// Class that generates HTML code
// Generated  2014-06-01 16:31:20
// Author: Ali
// =====================================================================================
class clsHtml {
    
    /** HTML Elements */
    /** ------------------------------------------ */
    
    /**
     * Returns htmlOptions
     * @param string array $arHtmlOptions key => value
     * @return string
     */
    protected static function getHtmlOptions($arHtmlOptions =  array()) {
        $str = " ";
        /* Check if not set in some cases */
        if(!isset($arHtmlOptions['class'])) {
            $arHtmlOptions['class']='';
        }
        
        // check if textField is rquired
        if(isset($arHtmlOptions['required'])) {
            unset($arHtmlOptions['required']); // remove html5/browser based popup
        }
        
        /** Check for attribute 'append-class' */
        if(isset($arHtmlOptions['append-class'])) {
            $arHtmlOptions['class'] .= ' ' . $arHtmlOptions['append-class']; // append to css-class
            unset($arHtmlOptions['append-class']); // delete it so foreach below wont set it 
        }
        
        foreach ($arHtmlOptions as $strAttName=>$strAttVal) {
            $str .= "{$strAttName}='{$strAttVal}' ";
        }
        
        return $str;
    }


    /**
     * @param string $strMsg
     * @param string array $arHtmlOptions key value pair (att for html element)
     * @return string
     */
    public static function span($strMsg, $arHtmlOptions =  array()) {
        
        if(!isset($arHtmlOptions['class'])) $arHtmlOptions['class']="help-block";
        
        $str = "<span ";
        $str .= self::getHtmlOptions($arHtmlOptions);
        $str .=" >";
        $str .= ($strMsg);
        $str .= '</span>';
        
        return $str;
    }
    
    public static function b($strMsg, $arHtmlOptions=[]) {

        $str = "<b ";
        $str .= self::getHtmlOptions($arHtmlOptions);
        $str .=" >";
        $str .= $strMsg;
        $str .= '</b>';

        return $str;
    }

    /**
     * Returns HTML of BS 3 alert
     * 
     * @param string $strMsg the error message (s)
     * @param boolean $boolDismiss if true show close button
     * @param array $arHtmlOptions html attributes for alert DIV 
     * 
     * @return string 
     */
    public static function alert($strMsg, $boolDismiss=false, $arHtmlOptions=[]) {
        
        if(empty($strMsg)) return ''; // hack to avoid showing blank div 
        
        if(!isset($arHtmlOptions['class'])) $arHtmlOptions['class']="alert alert-danger";
        
        $str = "<div ";
        $str .= self::getHtmlOptions($arHtmlOptions);
        $str .=" >";
        $str .= $boolDismiss ? '<a href="#" class="close" data-dismiss="alert">&times;</a>' : '';
        $str .= ($strMsg);
        $str .= '</div>';
        
        return $str;
    }
    
    /**
     * Genrates HTML for DIV
     * 
     * @param array $arHtmlOptions key value html element attributes eg class, id
     * @param sting $strContent content of div 
     * @param boolen $boolEnd if true the closing tag </div> is also generated
     * 
     * @return string HTML for required div
     */
    public static function startDiv($arHtmlOptions = [], $strContent='', $boolEnd=false) {
        
        $str = "<div ";
        $str .= self::getHtmlOptions($arHtmlOptions);
        $str .=" >";
        $str .= $strContent;
        $str .= $boolEnd ? self::endDiv():'';
        
        return $str;
    }
    
    /**
     * Generated the closing div tag 
     * 
     * @return string html of closing tag
     */
    public static function endDiv() {
        $str = '</div>';
        
        return $str;
    }
    
    public static function spanCol($strHeading, $strMsg, $arHtmlOptions =  array()) {
        return '<b>'.$strHeading.'</b>'.self::span($strMsg, $arHtmlOptions);
    }
    
    public static function a($strLabel, $strHref, $arHtmlOptions =  array()) {
        $str = "<a ";
        $str .= self::getHtmlOptions($arHtmlOptions);
        $str .=" href='{$strHref}'>";
        $str .= $strLabel;
        $str .= '</a>';
        
        return $str;
    }
    
    public static function label($strLabel, $arHtmlOptions =  array()) {
        $str = "<label ";
        $str .= self::getHtmlOptions($arHtmlOptions);
        $str .=" >";
        $str .= $strLabel;
        $str .= '</label>';
        
        return $str;
    }
    
    public static function p($strContent, $arHtmlOptions=[]) {
        $str = "<p ";
        $str .= self::getHtmlOptions($arHtmlOptions);
        $str .=" >";
        $str .= $strContent;
        $str .= '</p>';
        
        return $str;
    }
    
    public static function h($intLevel, $strContent, $arHtmlOptions=[]) {
        $strHLevel = "h{$intLevel}";
        $str = "<{$strHLevel} ";
        $str .= self::getHtmlOptions($arHtmlOptions);
        $str .=" >";
        $str .= $strContent;
        $str .= "</{$strHLevel}>";
        
        return $str;
    }
    
    /**
     * @param string $strMsg
     * @param string array $arHtmlOptions key value pair
     * @return string
     */
    public static function strong($strMsg, $arHtmlOptions =  array()) {
        $str = "<strong ";
        $str .= self::getHtmlOptions($arHtmlOptions);
        $str .=" >";
        $str .= addslashes($strMsg);
        $str .= '</strong>';
        
        return $str;
    }
    
    public static function submitButton($strLabel,$arHtmlOptions =  array()) {
        if(!isset($arHtmlOptions['class'])) {
            $arHtmlOptions['class']='btn btn-primary';
        }
        return self::getButton('submit', $strLabel, $arHtmlOptions);
    }
    
    public static function submitButtonRow($strLabel='Spara',$arHtmlOptions =  array()) {
        $str = '<div class="form-group">';
            $str .= self::submitButton($strLabel,$arHtmlOptions);
        $str .= '</div>';
        
        return $str;
    }
    
    public static function resetButton($strLabel,$arHtmlOptions =  array()) {
        
        if(!isset($arHtmlOptions['class'])) {
            $arHtmlOptions['class']='btn btn-default';
        }
        return self::getButton('reset', $strLabel, $arHtmlOptions);
    }
    
    public static function button($strType, $strLabel, $arHtmlOptions =  array()) {
        return self::getButton($strType, $strLabel, $arHtmlOptions);
    }
    
    protected static function getButton($strType, $strLabel, $arHtmlOptions =  array()) {
        
        // set default class
        if(!isset($arHtmlOptions['class'])) {
            $arHtmlOptions['class']='btn btn-primary';
        }
        
        $str = "<button type='{$strType}'";
        $str .= self::getHtmlOptions($arHtmlOptions);
        $str .=" >";
        $str .= addslashes($strLabel);
        $str .= '</button>&nbsp;';
        
        return $str;
    }
    
    
    /**
     * Generates BS3 based checkbox
     * 
     * @param string $strName
     * @param string $strValue
     * @param string $strLabel
     * @param string $arHtmlOptions
     * 
     * @return string
     */
    public static function checkBox($strName, $strValue='', $checked=false, $strLabel='', $arHtmlOptions =  array()) {

        // set default id
        $strId='';
        if(!isset($arHtmlOptions['id'])) {
            $strId="id='input_{$strName}'"; // sets id=input_strName by default
        }
        
        // checked ?
        $strChecked='';
        if($checked) {
            $strChecked='checked';
        }
        
        $str ='<label class="checkbox-inline">';
            $str .= "<input type='checkbox' name='{$strName}' value=\"{$strValue}\" class='checkbox' {$strId} $strChecked ";
                $str .= self::getHtmlOptions($arHtmlOptions); // make checkbox attributes eg id, class, style etc
            $str .=" />";
            $str .=$strLabel;
         $str .='</label>';
        
        return $str;
    }
    
    public static function radioField($strName, $strValue='', $checked=false, $strLabel='', $arHtmlOptions =  array()) {

        // set default id
        $strId='';
        if(!isset($arHtmlOptions['id'])) {
            $strId="id='input_{$strName}'"; // sets id=strName by default
        }
        
        // checked ?
        $strChecked='';
        if($checked) {
            $strChecked='checked';
        }
        
        $str ='<label class="checkbox-inline">';
            $str .= "<input type='radio' name='{$strName}' value=\"{$strValue}\" class='checkbox-inline' {$strId} $strChecked ";
                $str .= self::getHtmlOptions($arHtmlOptions); // make checkbox attributes eg id, class, style etc
            $str .=" />&nbsp; ";
            $str .=$strLabel;
         $str .='</label>';
        
        return $str;
    }
    
    /**
     * Generates BS3 based Html for text Field/hidden Field
     * 
     * @param string $strName The name of the textField
     * @param string $strValue The value of textField
     * @param array $arHtmlOptions any attributes for textField eg id, placeholder etc
     * 
     * @return string the html for text field 
     */
    public static function textField($strName, $strValue='', $arHtmlOptions =  array()) {
        
        // set default id
        $strId='';
        $strClass = "form-control";
        if(!isset($arHtmlOptions['id'])) {
            $strId="id='input_{$strName}'"; // sets id=strName by default
        }
        
        // check if type is text or hidden or email
        $type='text';
        if(isset($arHtmlOptions['type'])) {
            $type=$arHtmlOptions['type'];
        }
        
        if(isset($arHtmlOptions['class'])) {
            $strClass .= " ". $arHtmlOptions['class'];
        }
        
        $str = "<input type='{$type}' name='{$strName}' value=\"{$strValue}\" class='{$strClass}' {$strId} ";
            $str .= self::getHtmlOptions($arHtmlOptions); // make text attributes eg id, placeholder , style etc
        $str .=" />";
        
        return $str;
    }
    
    public static function hiddenField($strName, $strValue='', $arHtmlOptions=[]) {
        $arHtmlOptions['type']='hidden';
        return self::textField($strName, $strValue, $arHtmlOptions);
    }
    
    public static function emailField($strName, $strValue='', $arHtmlOptions =  array()) {
        $arHtmlOptions['type']='email';
        return self::textField($strName, $strValue, $arHtmlOptions);
    }
    
    public static function emailFieldRow($strName, $strValue, $strLabel, $strHelpBlock, $strError='', $arHtmlOptions =  array()) {
        $arHtmlOptions['type']='email'; // set type
        return self::textFieldRow($strName, $strValue, $strLabel, $strHelpBlock, $strError, $arHtmlOptions);
    }
    
    public static function passwordFieldRow($strName, $strValue, $strLabel, $strHelpBlock, $strError='', $arHtmlOptions =  array()) {
    	$arHtmlOptions['type']='password'; // set type
    	return self::textFieldRow($strName, $strValue, $strLabel, $strHelpBlock, $strError, $arHtmlOptions);
    }
    
    /**
     * Generates BS3 based Html for text area
     * 
     * @param string $strName The name of the text area
     * @param string $strValue The value of text area
     * @param array $arHtmlOptions any attributes for text area eg id, placeholder etc
     * 
     * @return string the html for text area 
     */
    public static function textArea($strName, $strValue='', $arHtmlOptions =  array()) {
        
        // set default id
        $strId='';
        if(!isset($arHtmlOptions['id'])) {
            $strId="id='input_{$strName}'"; // sets id=strName by default
        }
        
        // set defaults
        $strRows='';
        if(!isset($arHtmlOptions['rows'])) {
            $strRows= "rows='4'";
        } // else it would be set by func getHtmlOptions() below
        
        $strCols='';
        if(!isset($arHtmlOptions['cols'])) {
            $strCols= "cols='50'";
        } // else it would be set by func getHtmlOptions() below
        
        $strClass = '';
        if(isset($arHtmlOptions['class'])){
        	$strClass = $arHtmlOptions['class'];
        }
        
        $str = "<textarea name='{$strName}' class='form-control {$strClass}' {$strId} {$strRows} {$strCols} ";
            $str .= self::getHtmlOptions($arHtmlOptions); // make text attributes eg id, placeholder , style etc
        $str .=" >";
        $str .=$strValue;
        $str .="</textarea>";
        
        return $str;
    }
    
    public static function textAreaRow($strName, $strValue, $strLabel, $strHelpBlock, $strError='', $arHtmlOptions =  array()) {
        $strField = self::textArea($strName, $strValue, $arHtmlOptions);
        $str = self::fieldRow($strField, $strName, $strValue, $strLabel, $strHelpBlock,$strError, $arHtmlOptions);
        
        return $str;
    }
    
    /**
     * Generates BS3 based Html for textField row (with div and help block etc)
     * 
     * @param string $strName The name of the textField
     * @param string $strValue The value of textField
     * @param string $strLabel The label for textField
     * @param string $strHelpBlock The help text for textField
     * @param string $strError If the textField has validation error, empty if no error 
     * @param array $arHtmlOptions any attributes for textField eg id, placeholder etc
     * 
     * @return string the html for row
     */
    public static function textFieldRow($strName, $strValue, $strLabel, $strHelpBlock, $strError='', $arHtmlOptions =  array()) {
        
        $strField = self::textField($strName, $strValue, $arHtmlOptions);
        $str = self::fieldRow($strField, $strName, $strValue, $strLabel, $strHelpBlock,$strError, $arHtmlOptions);
        
        return $str;
    }
    
    protected static function fieldRow($strField, $strName, $strValue, $strLabel, $strHelpBlock, $strError='', $arHtmlOptions =  array()) {
        
        // check if textField is rquired
        if(isset($arHtmlOptions['required'])) {
            $strLabel .='*';
        }
        
        // check if text/any field has error
        $strHasError='';
        if(!empty($strError)) {
            $strHasError = 'has-error';
            $strHelpBlock = $strError; // overide help msg with error msg
        }
        
        // if label is empty then dont show :
        if(!empty($strLabel)) $strLabel = $strLabel . ':';
        
        $strClass = "";
        if(isset($arHtmlOptions['class'])){        	
        	$strClass = $arHtmlOptions['class'];
        }
        
        
        // make row html
        $str = "<div class='form-group {$strHasError} {$strClass}'>";
            $str .= "<label for='{$strName}' class='control-label'>{$strLabel}</label>";
            $str .= $strField;
            $str .= empty($strHelpBlock) ? '':"<span class='help-block'>({$strHelpBlock})</span>";
         $str .="</div>";
        
        return $str;
    }
    
    public static function checkBoxRow($strName, $booleanValue, $strLabel, $strHelpBlock='', $strError='', $arHtmlOptions =  array()){
    	// check if text field has error
    	$strIsChecked = '';
    	$strHasError='';
    	if(!empty($strError)) {
    		$strHasError = 'has-error';
    		$strHelpBlock = $strError; // overide help msg with error msg
    	}
    	
    	if(is_bool($booleanValue) && $booleanValue == true){
    		$strIsChecked = 'checked="checked"';
    	}
    	if(!empty($strError)){
    		$str = '<div class="'.$strHasError.'">';
    	}
        
        $value = isset($arHtmlOptions['value']) ? $arHtmlOptions['value'] : 1;
        
    	$str = '<div class="checkbox">';
    	$str .= '<label>';
    	$str .= '<input type="checkbox" class="checkbox" name="'.$strName.'" value="'.$value.'" '.$strIsChecked.' />';
    	$str .= $strLabel;
    	$str .= '</label>';
    	$str .= "</div>";

    	if(!empty($strError)){
    		$str .= '</div>';
    	}
    	
    	return $str;
    }
    
    /**
     * Returns ul of an array
     * 
     * @param string $arLi array of <li> elements, value is the label
     * @param type $arHtmlOptions key value pair
     * @param type $showKey shows key of assoc array, for debug
     * @return string
     */
    public static function ul($arLi, $showKey=false, $arHtmlOptions =  array()) {
        $str = "<ul ";
        $str .= self::getHtmlOptions($arHtmlOptions);
        $str .=" >";
        foreach ($arLi as $strKey=>$strVal){
            $str .= '<li>';
                $str .= $showKey ? addslashes($strKey) . ': ' . addslashes($strVal) : ($strVal);
            $str .= '</li>';
        }
        $str .= '</ul>';
        
        return $str;
    }
    
    /**
     * Returns HTML Table from a two-dim array
     * 
     * @param array $arTable
     * @param bool $showKey
     * @param array $arHtmlOptions attributes of the html element
     * @return string html of table
     */
    public static function table($arTable, $showKey=false, $arHtmlOptions =  array()) {
        $str = '<table ';
        $str .= self::getHtmlOptions($arHtmlOptions);
        $str .=' >';
        foreach ($arTable as $arRow){ // generate tr
            $str .="<tr>"; 
                foreach ($arRow as $strKey=>$strVal){ // generate td
                    $strVal = empty($strVal)? '&nbsp;--&nbsp;':$strVal;
                    $str .= '<td nowrap>';
                        $str .= $showKey ? ($strKey) . ': ' . ($strVal) : ($strVal);
                    $str .= '</td>';
                }
            $str .="</tr>";
        }
        $str .= '</table>';
        
        return $str;
    }
    
    /**
     * Returns HTML select box 
     * 
     * @param string $strName name of select box
     * @param array $arOptions key value pair for option 
     * @param sting $selected the selected value in the option
     * @param array $arHtmlOptions attributes of the html element
     * @return string html of select box
     */
    public static function select($strName, $arOptions, $selectd=false,  $arHtmlOptions =  array(), $selecte_class = 'form-control') {
        $str = "<select class='".$selecte_class."' name='{$strName}'";
        $str .= self::getHtmlOptions($arHtmlOptions);
        $str .=' >';
            foreach ($arOptions as $strValue=>$strLabel){ // generate options
                $str .="<option "; 
                if($strValue==$selectd) $str .= ' selected="" ';
                $str .= " value=\"{$strValue}\">";
                $str .= $strLabel;
                $str .="</option>";
            }
            
        $str .= '</select>';
        
        return $str;
    }
    
    
    public static function selectFieldRow($strName, $arOptions, $strLabel, $strHelpBlock, $selectedValue=false, $strError='', $arHtmlOptions =  array(), $arSelectHtmlOptions =  array()) {
        
        $strField = self::select($strName, $arOptions, $selectedValue, $arSelectHtmlOptions);
        $str = self::fieldRow($strField, $strName, '', $strLabel, $strHelpBlock, $strError, $arHtmlOptions);
        
        return $str;
    }
    
    
    /**
     * Generates the opening tag for form
     * 
     * @param string $strActionUrl url for form action 
     * @param string $strId id for the form 
     * 
     * @return string
     */
    public static function startForm($strActionUrl='', $strId='form_id', $arHtmlOptions=[]) {
        
        $strResult ="<div class=\"trisoftStarForm\">\n";
            $strResult .="<form ".self::getHtmlOptions($arHtmlOptions)." id='{$strId}' action=\"{$strActionUrl}\" method=\"POST\" enctype=\"multipart/form-data\">\n" ;

            return $strResult;
    }
    
    /**
     * Generates closing tag for form with/without submit button
     * 
     * @param string $strSubmitLabel The label for Submit button, defaults to 'Spara'
     * @param boolen $boolShowSubmit Wether to generate submit button
     * @param array $arHtmlOptions htmloptions for submit button
     * 
     * @return string
     */
    public static function endForm($boolShowSubmit=true,$strSubmitLabel='Spara', $arHtmlOptions=[]) {
                $value = isset($arHtmlOptions['value']) ? $arHtmlOptions['value'] : 'Save!';
                $strResult = clsHtml::textField('action', $value, array('type'=>'hidden'));
                $strResult .= '<br />';
                if($boolShowSubmit) {
                    //  $strResult .= '<hr />'; // some space
                    $strResult .= clsHtml::submitButtonRow($strSubmitLabel,$arHtmlOptions);
                }
                        
            $strResult .= "</form>";
		$strResult .= "</div>"; // end of trisoftStarForm
        
        return $strResult;
    }
    
}
