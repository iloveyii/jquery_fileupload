$(document).on('ready', function() {
    $("#input-id").fileinput({
        showUpload: false,
        maxFileCount: 2,
        mainClass: "input-group-lg",
        overwriteInitial:false,
        uploadUrl:'/site/file-upload',
        showCaption: true
    });
});

